import character from '../Gameplay/character';
const { ccclass, property } = cc._decorator;

@ccclass
export default class menu extends cc.Component {
    is_debugging: boolean = false;
    @property(cc.Node)
    join_match_button: cc.Node = null

    @property(cc.Node)
    menu_text: cc.Node = null

    static instance: menu = null;

    onLoad() {
        menu.instance = this;
        if (this.is_debugging) {
            this.start_game_debug()
        }

    }

    start() {

    }

    // update (dt) {}
    set_wait_for_another_user() {
        console.log("waiting for another user mode")
        this.join_match_button.getComponent(cc.Button).enabled = false
        this.join_match_button.color = cc.Color.BLACK
        this.join_match_button.opacity = 200

        this.menu_text.getComponent(cc.Label).string = "Waiting for another player to join ... "
    }
    start_game() {
        this.node.destroy();
    }
    start_game_debug() {
        character.instance.initialize_game()
        this.start_game()
    }
}
