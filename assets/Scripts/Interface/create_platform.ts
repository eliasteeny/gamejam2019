// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    debug_physics: boolean = false;

    @property(cc.Node)
    platform_parent: cc.Node = null;

    @property(cc.SpriteFrame)
    ground_clay: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    ground_grass: cc.SpriteFrame = null;


    @property(cc.Prefab)
    tile_prefab: cc.Prefab = null;

    scale: number = 0.3;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        cc.director.getCollisionManager().enabled = true;
        cc.director.getCollisionManager().enabledDebugDraw = true;
        cc.director.getCollisionManager().enabledDrawBoundingBox = true;
        cc.director.getPhysicsManager().enabled = true;


        if (this.debug_physics) {

            cc.director.getPhysicsManager().debugDrawFlags = cc.PhysicsManager.DrawBits.e_aabbBit |
                cc.PhysicsManager.DrawBits.e_pairBit |
                cc.PhysicsManager.DrawBits.e_centerOfMassBit |
                cc.PhysicsManager.DrawBits.e_jointBit |
                cc.PhysicsManager.DrawBits.e_shapeBit
                ;
        }
        // 0 = clay ground
        // 1 = grass ground
        // 2 = deep hill clay
        // 3 = deep hill grass
        // 4 = normal hill clay
        // 5 = normal hill grass
        // let platform = [
        //     [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1],
        //     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        //     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        // ]

        // let starting_x = -this.node.width / 2;


        // for (let i = 0; i < platform.length; i++) {
        //     for (let j = 0; j < platform[i].length; j++) {
        //         let node = cc.instantiate(this.tile_prefab);
        //         node.scale = this.scale;
        //         node.parent = this.platform_parent

        //         // node.parent = this.node




        //         if (platform[i][j] === 0) {
        //             node.getComponent(cc.Sprite).spriteFrame = this.ground_clay;
        //             node.x = starting_x + (this.ground_clay.getOriginalSize().width * this.scale / 2) + (j * this.ground_clay.getOriginalSize().width * this.scale)
        //             node.y = 0 - (this.ground_clay.getOriginalSize().height * this.scale * i)
        //         }
        //         if (platform[i][j] === 1) {
        //             node.getComponent(cc.Sprite).spriteFrame = this.ground_grass;
        //             node.x = starting_x + (this.ground_clay.getOriginalSize().width * this.scale / 2) + (j * this.ground_grass.getOriginalSize().width * this.scale)
        //             node.y = 0 - (this.ground_grass.getOriginalSize().height * this.scale * i)
        //         }




        //         // node.getComponent(cc.PhysicsBoxCollider).size.width = this.ground_clay.getOriginalSize().width
        //         // node.getComponent(cc.PhysicsBoxCollider).size.height = this.node.height
        //     }
        // }


        // console.log("polugen : ")
        // let test = this.generate_platform();
        // console.log("testing array : ")
        // console.log(test)
    }

    start() {

    }
    // generate_platform() {
    //     let platform = [];
    //     platform[0] = 0;
    //     platform[1] = 0;
    //     platform[2] = 0;

    //     for (let i = 3; i < 250; i++) {
    //         let random_number: number = Math.random();
    //         // if(random_number[i] ===  random_number[i-1] === 0 && random_number[i-2] === 0 && ra)
    //         if (platform[i - 1] === platform[i - 2]) {
    //             if (random_number < 0.5) {
    //                 platform[i] = 0;
    //             } else {
    //                 platform[i] = -1
    //             }
    //         } else {
    //             if (random_number < 0.5) {
    //                 platform[i] = platform[i - 1] + 1;
    //             } else {
    //                 platform[i] = platform[i - 1] - 1;
    //             }
    //         }
    //     }
    //     return platform
    // }

    // update (dt) {}
}
