import * as nakama from "@heroiclabs/nakama-js"
import { Socket, DefaultSocket } from "@heroiclabs/nakama-js/dist/socket";
import backend from "../Managers/BackendManager";
import multiplayer from "../Managers/MultiplayerRPCManager";




const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    join_match: cc.Node = null;

    @property(cc.Node)
    send: cc.Node = null;


    onLoad() {
        // var client = new nakama.Client("defaultkey", "167.71.74.165", "7350");
        // client.ssl = false;

        // const customId = "some-custom-id";
        // const sessionn = client.authenticateCustom({ id: customId, create: true, username: "mycustomusername" }).then((session) => {
        //     console.info("Successfully authenticated:", session);
        // }).catch((error) => {
        //     console.log("error : ")
        //     console.log(error)
        // });
        backend.init();

        this.join_match.on(cc.Node.EventType.TOUCH_START, (event) => {
            if (this.join_match.getComponent(cc.Button).enabled)
                multiplayer.TEST_find_and_join_or_create_quick_game();
        });

        this.send.on(cc.Node.EventType.TOUCH_START, (event) => {
            multiplayer.send_data(3)
        })

    }

    start() {

    }

    // update (dt) {}
}
