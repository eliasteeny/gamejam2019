import character from '../Gameplay/character';
import multiplayer from '../Managers/MultiplayerRPCManager';

const { ccclass, property } = cc._decorator;

@ccclass
export default class dynamic extends cc.Component {

    static instance: dynamic = null;

    @property(cc.Node)
    platform_earth: cc.Node = null;

    @property(cc.Node)
    platform_alien: cc.Node = null;

    @property(cc.Node)
    character_human: cc.Node = null;

    @property(cc.Node)
    portal: cc.Node = null;

    @property(cc.Node)
    power_up_send_rock: cc.Node = null;

    @property(cc.Node)
    power_up_send_rock2: cc.Node = null;

    @property(cc.Prefab)
    rock: cc.Prefab = null;

    @property(cc.SpriteFrame)
    alien_sprite: cc.SpriteFrame

    @property(cc.Node)
    boost: cc.Node

    portal_animation: cc.Animation = null;

    power_up_send_rock_animation: cc.Animation = null;

    power_up_send_rock2_animation: cc.Animation = null;

    onLoad() {
        dynamic.instance = this;

        this.portal_animation = this.portal.getComponent(cc.Animation)

        this.power_up_send_rock_animation = this.power_up_send_rock.getComponent(cc.Animation)

        this.power_up_send_rock2_animation = this.power_up_send_rock2.getComponent(cc.Animation)

        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, (event) => {

            if (event.keyCode == cc.macro.KEY.c) {
                this.open_portal()
            }

        })



    }

    start() {

    }

    open_portal() {
        this.portal.x = character.instance.node.x + this.node.parent.width / 3;
        this.portal.y = character.instance.node.y;

        let animation_state: cc.AnimationState = this.portal_animation.play('open_portal')



    }

    throw_rock() {
        console.log("throwing rock ...")
        let rock = new cc.Node;
        rock = cc.instantiate(this.rock)
        rock.parent = this.node.parent
        rock.x = this.portal.x;
        rock.y = this.portal.y;
    }

    take_power_up_send_rock() {
        this.power_up_send_rock_animation.play('take_power_up')
        multiplayer.send_data(0);
    }

    take_power_up2_send_rock() {
        this.power_up_send_rock2_animation.play('take_power_up')
        multiplayer.send_data(0);
    }

    apply_alien_mode() {
        this.platform_alien.active = true;
        this.platform_earth.active = false;

        this.character_human.getComponent(cc.Sprite).spriteFrame = this.alien_sprite
        // this.character_human.active = false;
    }

    apply_human_mode() {
        this.platform_alien.active = false;
        this.platform_earth.active = true;

        this.character_human.active = true;
    }

    apply_boost_send_down_boost() {
        character.instance.node.getComponent(cc.RigidBody).applyLinearImpulse(cc.v2(15000, 0), cc.v2(0, 0), true)
        multiplayer.send_data(1)
    }

    apply_down_boost() {
        character.instance.node.getComponent(cc.RigidBody).applyLinearImpulse(cc.v2(-15000, 0), cc.v2(0, 0), true)
    }
    update(dt) {
        // if(cc.Intersection.rectRect())
    }
}
