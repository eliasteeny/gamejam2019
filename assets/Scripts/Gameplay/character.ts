import dynamic from '../Gameplay/dynamics';
import multiplayer from '../Managers/MultiplayerRPCManager';
const { ccclass, property } = cc._decorator;

@ccclass
export default class character extends cc.Component {
    static instance: character = null;

    @property(cc.Node)
    camera: cc.Node = null;

    onclick_counter: number = 1


    right_button_down: boolean = false;
    left_button_down: boolean = false;

    jumped: boolean = false;
    animation: cc.Animation = null;
    button_pressed: boolean = false;

    onLoad() {
        character.instance = this;
        this.animation = this.node.getComponent(cc.Animation);
    }

    start() {

    }

    update(dt) {
        this.camera.x = this.node.x;
        this.camera.y = this.node.y + this.node.parent.height / 4;
        if (this.right_button_down) {
            this.node.getComponent(cc.RigidBody).applyLinearImpulse(cc.v2(500, 0), cc.v2(0, 0), true)
        }

        if (this.left_button_down) {
            this.node.getComponent(cc.RigidBody).applyLinearImpulse(cc.v2(-500, 0), cc.v2(0, 0), true)
        }


    }

    initialize_game() {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, (event) => {

            if (event.keyCode == cc.macro.KEY.right) {
                if (!this.button_pressed) {
                    console.log("right click is pressed on character : ")
                    if (multiplayer.player_1) {
                        this.animation.play('running_human')
                    } else {
                        this.animation.play('running_alien')
                    }

                    this.node.scaleX = 1
                    this.right_button_down = true
                    this.button_pressed = true;
                }

            }

            if (event.keyCode == cc.macro.KEY.left) {
                if (!this.button_pressed) {
                    if (multiplayer.player_1) {
                        this.animation.play('running_human')
                    }
                    else {
                        this.animation.play('running_alien')
                    }
                    this.node.scaleX = -1;
                    this.left_button_down = true
                    this.button_pressed = true;
                }

            }

            if (event.keyCode == cc.macro.KEY.space) {
                if (!this.jumped) {
                    this.node.getComponent(cc.RigidBody).applyLinearImpulse(cc.v2(0, 10000), cc.v2(0, 0), true)
                    this.jumped = true;
                }
            }

        })

        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, (event) => {
            this.right_button_down = false;
            this.left_button_down = false;
            this.onclick_counter = 0;
            this.jumped = false
            this.button_pressed = false;

            this.animation.stop('running_human')
            this.animation.stop('running_alien')
        })
    }

    contacted_with_power_up_send_rock: boolean = false;
    contacted_with_power_up_send_rock2: boolean = false;

    contacted_with_boost: boolean = false;

    onBeginContact(contact, selfCollider, otherCollider) {
        if (otherCollider.node == dynamic.instance.power_up_send_rock && !this.contacted_with_power_up_send_rock) {
            console.log("coliding with powerup")
            dynamic.instance.take_power_up_send_rock()
            this.contacted_with_power_up_send_rock = true;
        }

        if (otherCollider.node == dynamic.instance.power_up_send_rock2 && !this.contacted_with_power_up_send_rock2) {
            console.log("coliding with powerup")
            dynamic.instance.take_power_up2_send_rock()
            this.contacted_with_power_up_send_rock2 = true;
        }

        if (otherCollider.node == dynamic.instance.boost && !this.contacted_with_boost) {
            console.log("colliding with boost")
            dynamic.instance.apply_boost_send_down_boost()
            this.contacted_with_boost = true
        }
    }

}
