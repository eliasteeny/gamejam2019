import * as nakama from "@heroiclabs/nakama-js"
import { Socket, DefaultSocket } from "@heroiclabs/nakama-js/dist/socket";
import multiplayer from "./MultiplayerRPCManager";


const useSSL = false;
const verboseLogging = true;
const domain = "167.71.74.165"; // "ec2-18-225-18-171.us-east-2.compute.amazonaws.com";
// const domain = "testing.backend.groovyantoid.com"; // "ec2-18-225-18-171.us-east-2.compute.amazonaws.com";

const client: nakama.Client = new nakama.Client("defaultkey", domain, "7350");
client.ssl = useSSL;
const socket = client.createSocket(useSSL, verboseLogging);
const createStatus = false;


enum signals {
    init_complete
}

class BackendManager {

    session = null;

    static readonly signals = signals;

    node: cc.Node = new cc.Node("Backend");

    get signals() {
        return signals;
    }

    init() {
        // const sessionn = client.authenticateCustom({ id: "some0cdcsdsdded", create: true }).then((session) => {
        //     console.info("Successfully authenticated:", session);
        // }).catch((error) => {
        //     console.log("error : ")
        //     console.log(error)
        // });
        console.log("Constructed: BACKEND MANAGER.");

        this.restoreSessionOrAuthenticate().then(() => {
            console.log("restoreSessionOrAuthenticate finished successfully.");
            try {
                this.node.emit(signals.init_complete.toString());



                // multiplayer.find_and_join_or_create_quick_game();create_public_match


            } catch (e) {
                console.error("Unit tests failed with error %o", e);

            }

        }).catch((err) => {
            console.error("restoreSessionOrAuthenticate failed with error %o ", err);
        });
    }

    get client(): nakama.Client {
        return client;
    }

    get socket() {
        return socket;
    }


    start() {

    }

    storeSession(session) {
        console.log("> USER ID is " + String(session.user_id));
        if (typeof (Storage) !== "undefined") {
            localStorage.setItem("nakamaToken", session.token);
        } else {
            // We'll assume this is a React Native project.
            AsyncStorage.setItem('@MyApp:nakamaToken', session.token).then(function (session) {
            }).catch(function (error) {
                console.log("Nakama: An error occured while storing session: %o", error);
            });
        };
    }

    async getSessionFromStorage() {
        if (typeof (Storage) !== "undefined") {
            return Promise.resolve(localStorage.getItem("nakamaToken"));
        } else {
            return Promise.reject("Storage object not found.");

            // try {
            //     // Example assumes you use React Native.
            //     return AsyncStorage.getItem('@MyApp:nakamaToken');
            // } catch (e) {
            //     console.log("Could not fetch data, error: %o", e);
            // }
        }
    }

    // WARNING: This needs to be called somewhere if it is not called in the constructor.
    async restoreSessionOrAuthenticate() {
        try {
            var customId = (Math.random() * 1212332).toString();

            console.log("customID is " + String(customId));

            var sessionString = null; //await this.getSessionFromStorage();
            if (sessionString && sessionString != "") {
                this.session = nakama.Session.restore(sessionString);
                var currentTimeInSec = new Date().getTime() / 1000;
                if (!this.session.isexpired(currentTimeInSec)) {
                    this.session = await socket.connect(this.session, createStatus);
                    console.log("Nakama: Socket connected successfully.");

                    multiplayer.connect_socket(socket);

                    this.storeSession(this.session);

                    return Promise.resolve(this.session);
                }
            }

            console.log("Nakama: Attempting to authenticate with " + customId);
            // this.session = client.authenticateCustom({ id: customId, create: true })
            this.session = await client.authenticateCustom({
                id: customId,
                create: true,
            });

            console.log("Nakama: authenticated");

            this.session = await socket.connect(this.session, createStatus);
            console.log("Nakama: Socket connected successfully.");

            multiplayer.connect_socket(socket);

            this.storeSession(this.session);

            return Promise.resolve(this.session);
        } catch (e) {
            console.log("Nakama: An error occured while trying to restore session or authenticate user: %o", e)
            return Promise.reject(e);
        }
    }


    async update_personal_account(updates) {
        await client.updateAccount(this.session, updates);

        console.info("Nakama: Successfully updated account with: ", updates);
    }

    async fetch_personal_account() {
        const account = await client.getAccount(this.session);

        return account || null;
    }

    async fetch_public_accounts(ids = [], usernames = []) {

        const users_object = await client.getUsers(this.session, ids, usernames, []);
        if (users_object) {
            users_object.users.forEach(user => {
                console.info("User id '%o' and display name '%o'.", user.id, user.display_name);
            })
        }

        return users_object.users;
    }

    async get_id_from_username(username) {
        // TODO Implement method.
        var id;

        return id;
    }

    async write_to_storage(collection, key, value) {
        const object_ids = await client.writeStorageObjects(this.session, [
            {
                "collection": collection,
                "key": key,
                "value": value,
                "permission_read": 2,
                "permission_write": 1
            }
        ]);
        console.info("Nakama: Successfully stored objects: ", object_ids);
    }

    async read_from_storage(collection, key, user_id = "") {
        // NOTE: This used to depend on the modified backend server to work, 
        // We added the argument `"user_id": session.user_id` to fix it and use a module where necessary.

        const storage_objects = await client.readStorageObjects(this.session, {
            "object_ids": [{
                "collection": collection,
                "key": key,
                "user_id": user_id || this.session.user_id
            }]
        });
        console.info("Nakama: Read objects: %o from " + collection, storage_objects);

        if (storage_objects && storage_objects.objects && storage_objects.objects[0]) {
            return storage_objects.objects[0]["value"];
        } else {
            console.error("Nakama: Returned storage object is null from: " + collection + " " + key);
            return null;
        }

    }

    async list_all_collection(collection_name: string, cursor: string = "") {

        const limit = 100; // default is 10.
        const objects_list = await client.listStorageObjects(this.session, collection_name, this.session.user_id, limit, cursor);
        console.info("Nakama: List objects: %o from " + collection_name, objects_list);

        return objects_list.objects;

    }


    async send_rpc(rpc_id: string, payload: object) {
        try {
            console.log("Nakama: Attempting to send rpc %o with payload %o", rpc_id, payload);

            var rpc_result = await client.rpc(this.session, rpc_id, payload);

            console.log("Nakama: Retrieved rpc result: %o", rpc_result);

            return rpc_result;
        } catch (e) {
            console.log("Nakama: Error sending rpc: %o", e);
            return null;

        }
    }

    async send_socket(payload) {
        try {

            // var whatever = await backendManager.restoreSessionOrAuthenticate();

            console.log("Nakama: Attempting to send socket data %o", payload);

            var socket_result = await socket.send(payload);

            console.log("Nakama: Retrieved socket result: %o", socket_result);

            return socket_result;
        } catch (e) {
            console.log("Nakama: Error sending socket data: %o", e);
            return null;
        }
    }


    async send_data_to_match(opCode, data) {

        var result = await this.send_socket({ match_data_send: { match_id: multiplayer.current_match_id, op_code: opCode, data: data } });

        return result;
    }

}

const backend = new BackendManager;

export default backend;