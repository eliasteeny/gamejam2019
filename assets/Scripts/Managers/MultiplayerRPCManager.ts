import backend from "./BackendManager";
import menu from "../Interface/menu";
import character from "../Gameplay/character";
import dynamic from "../Gameplay/dynamics";



enum signals {
    update_player_ids

}
class MultiplayerRPCManagerClass {



    static readonly signals = signals;

    player_1: boolean = false;
    player_2: boolean = false;

    node: cc.Node = new cc.Node("Multiplayer");

    get signals() {
        return signals;
    }

    current_match_id;
    match_state;

    constructor() {
        console.log("Constructed: MULTIPLAYER MANAGER.");

    }


    start() {

    }

    private _current_opponent_profile = null;
    set current_opponent_profile(value) {
        console.log("Opponent name: %s -- Set opponent profile as %o", value && value.display_name, value);
        this._current_opponent_profile = value;
        this.node.emit(MultiplayerRPCManagerClass.signals.update_player_ids.toString());
    }

    get current_opponent_profile() {
        return this._current_opponent_profile;
    }


    private _current_match_ids = [];
    set current_match_user_ids(value) {
        console.log("Updated current match ids %o", value);
        this._current_match_ids = value;

        for (let user_id in multiplayer.current_match_user_ids) {
            if (user_id !== backend.session.user_id) {

                backend.fetch_public_accounts([user_id]).then((users) => {
                    if (users && users[0]) {
                        multiplayer.current_opponent_profile = users[0];
                    }
                });


            }
        }

    }

    get current_match_user_ids() {
        return this._current_match_ids;
    }

    async find_quick_game(label = "") {
        var result = await backend.send_rpc("quick_find_match_rpc", { search_label: label });

        if (result && result.payload && result.payload.match_id) {
            console.log("Found quick game.");
            return result.payload.match_id;
        } else {
            console.log("Could not find quick game.");
            return null;
        }
    }

    async get_all_joined_matches() {
        return await backend.list_all_collection("joinedgames");
    }

    async autojoin_due_match_turn() {
        var matches = await this.get_all_joined_matches();
        for (var i = 0; i < matches.length; i++) {
            var match = matches[i];
            var op_state: number = match.value.op;
            switch (op_state) {
                case 1:
                    // Due bluffer
                    return await this.join_match(match.key);
                    // return true;
                    break;
                case 2:
                    // Due guesser
                    return await this.join_match(match.key);
                    // return true;
                    break;
                case 3:
                    // Waiting for opponent to play 
                    return false;
                case 4:
                    // Match is over, you lost.
                    return false;
                    break;
                case 5:
                    // Match is over, you won.
                    return false;
                    break;
                default:
                    break;
            }
        }
    }

    async TEST_find_and_join_or_create_quick_game(autojoin_due = false) {

        try {

            var result = await multiplayer.find_quick_game();

            if (result) {
                console.log("Recieved match ID %o, attempting to join.", result);

                var success = await multiplayer.join_match(result);
                if (success) {
                    console.log("Succesfully joined match.")
                } else {
                    console.log("Failed to join match.");
                }
            } else {

                console.log("NO match id, attempting to create new game.");
                var public_match = await multiplayer.create_public_match();
                if (public_match) {
                    console.log("Succesfully created public match.");

                    var joining = await multiplayer.join_match(public_match);

                    if (!joining) {
                        console.error("Could not join match.");


                    } else {
                        console.log("Saved match id as current.");

                    }
                } else {
                    console.log("Failed to create public match.")

                }

            }
        }
        catch (e) {
            console.log("Error while running multiplayer test: %o", e);
        }

    }
    async _create_match(match_payload: object) {
        var match_id = await backend.send_rpc("create_match_rpc", match_payload);

        if (match_id && match_id.payload && match_id.payload.match_id) {
            console.log("Succesfully created new match.");
            return match_id.payload.match_id;
        } else {
            console.log("Error creating new match.");
            return null;
        }

    }





    async create_public_match(payload = null) {
        var data = payload || { label: { visibility: "public" } };

        var match_id = await multiplayer._create_match(data);

        return match_id;
    }

    async join_match(a_match_id) {

        var match = await backend.send_socket({ match_join: { match_id: a_match_id } });

        if (match) {
            console.log("Joined match %o", match);
            if (match.presences) {
                console.log("Some presences found, printing...");

                var connectedOpponents = match.presences.filter((presence) => {
                    // Remove your own user from list.
                    return presence.user_id != match.self.user_id;
                });
                connectedOpponents.forEach((opponent) => {
                    console.log("User id %o, username %o.", opponent.user_id, opponent.username);
                });
            } else {
                console.log("No presences found...");

            }
            this.current_match_id = a_match_id;


            return match;
        } else {
            console.log("No match joined.");
            return null;
        }


        return match;
    }






    connect_socket(sock) {



        sock.onmatchdata = (result) => {
            console.log("match data triggered")
            multiplayer.handle_recieved_match_data(result);
        };

        // sock.onmatchpresence = (matchpresence) => {
        //     console.info("SOCKET: Received match presence update:", matchpresence);
        // };

        sock.ondisconnect = (event) => {
            console.info("SOCKET: Disconnected from the server. Event:", event);
        };

        sock.onerror = (event) => {
            console.info("SOCKET: Server error. Event:", event);
        };

    }

    handle_recieved_match_data(result) {
        var content = result.data;

        switch (result.op_code) {
            case 200: console.log("first user joined : ")
                menu.instance.set_wait_for_another_user()

                this.player_1 = true
                dynamic.instance.apply_human_mode()
                character.instance.initialize_game();
                break;
            case 201: console.log("another user joined : ")


                multiplayer.player_2 = true;


                if (!this.player_1) {
                    dynamic.instance.apply_alien_mode()
                }
                character.instance.initialize_game();
                menu.instance.start_game();
                break;
            case 2: console.log("message sent from other player")
                console.log(result.data.data)
                let json_recieved = JSON.parse(result.data.data)
                console.log(json_recieved)
                this.handle_recieved_id_op_code_2(parseInt(json_recieved.id))
                break;
        }

    }

    handle_recieved_id_op_code_2(id: number) {
        if (id === 0) {
            dynamic.instance.open_portal();
        }

        if (id === 1) {
            dynamic.instance.apply_down_boost()
        }
    }


    state_update(state) {
        multiplayer.match_state = state;
    }

    async send_data(id: number) {

        var opCode = 20;
        var data = {
            "id": id,
            "msg": "send rock"
        };

        backend.send_data_to_match(opCode, data);
    }


}

const multiplayer = new MultiplayerRPCManagerClass;

export default multiplayer;