local nk = require("nakama")

local function tsize(T)
    local count = 0
    for _ in pairs(T) do
        count = count + 1
    end
    return count
end

local function initialize_user(context, payload)
    nk.logger_info(("Hook after AuthenticateCustom"))
    if payload.created then
        -- Only run this logic if the account that has authenticated is new.
        local changeset = {
            coins = 100, -- Add 100 coins to the user's wallet.
            gems = 0 -- Add 5 gems from the user's wallet.
        }
        nk.logger_info(("Initializing player account with %q"):format(nk.json_encode(changeset)))
    -- nk.wallet_update(context.user_id, changeset, metadata, true)
    end
end

nk.register_req_after(initialize_user, "AuthenticateCustom")

local function create_match(context, payload)
    local modulename = "match_handler"

    local setupstate = {initialstate = nk.json_decode(payload)}

    local matchid = nk.match_create(modulename, setupstate)

    -- Send notification of some kind
    return nk.json_encode({["match_id"] = matchid, ["setupstate"] = setupstate})
end
nk.register_rpc(create_match, "create_match_rpc")

local min_size = 0
local max_size = 2
local search_limit = 100
local isAuthoritative = true
local function find_match(search_query)
    local matches = nk.match_list(search_limit, isAuthoritative, nil, min_size, max_size, search_query or "*")

    if (#matches > 0) then
        table.sort(
            matches,
            function(a, b)
                return a.size > b.size
            end
        )
        return matches[1].match_id
    else
        return nil
    end
end

local default_search_query = " +label.total_joined:<2"
local function quick_find_match(context, payload)
    local label = ((payload and nk.json_decode(payload).search_label) or "") .. default_search_query
    local found_match_id = find_match(label)
    nk.logger_info(("Quick find game result for %q is %q"):format(label))

    return nk.json_encode({["match_id"] = found_match_id})
end

nk.register_rpc(quick_find_match, "quick_find_match_rpc")

local function makematch(context, matched_users) -- ONLY for matchmaking
    -- print matched users
    for _, user in ipairs(matched_users) do
        local presence = user.presence
        nk.logger_info(("Matched user '%q' named '%q'"):format(presence.user_id, presence.username))
        for k, v in pairs(user.properties) do
            nk.logger_info(("Matched on '%q' value '%q'"):format(k, v))
        end
    end

    local modulename = "match_handler"
    local setupstate = {invited = matched_users}
    local matchid = nk.match_create(modulename, setupstate)
    return matchid
end
nk.register_matchmaker_matched(makematch)

local M = {}

local default_new_match_label = {visibility = "public"}
function M.match_init(context, setupstate)
    local tickrate = 5 -- per second, 1 might be enough
    local g_label
    local gamestate

    g_label = setupstate.initialstate.label or default_new_match_label
    g_label.total_joined = 0
    gamestate = {
        label = g_label,
        presences = {},
        registered_users = {},
        message_queue = {}
    }

    return gamestate, tickrate, nk.json_encode(g_label)
end

function M.match_join_attempt(context, dispatcher, tick, state, presence, metadata)
    local acceptuser = true

    local reason = ""

    if (state.complete) then
        acceptuser = false
        reason = "Match is over."
    elseif (tsize(state.registered_users) >= max_size and not state.registered_users[presence.user_id]) then
        -- Allow users to reconnect as long as they had legitimately registered.
        -- Do not allow users to join after max size is reached.
        acceptuser = false
        reason = "Match is full."
    end

    return state, acceptuser, reason
end

function M.match_join(context, dispatcher, tick, state, presences)
    for _, presence in ipairs(presences) do
        state.presences[presence.user_id] = presence

        state.label.total_joined = state.label.total_joined + 1
        dispatcher.match_label_update(nk.json_encode(state.label))

        if tsize(state.registered_users) < 1 then
            -- state.first_registered_time = os.time()
            state.tick_counter = 0
            state.registered_users[presence.user_id] = presence

            dispatcher.broadcast_message(200, nk.json_encode({["message"] = "First user has joined the match.", ["ids"] = state.registered_users}))
        elseif not state.registered_users[presence.user_id] then
            state.registered_users[presence.user_id] = presence

            dispatcher.broadcast_message(201, nk.json_encode({["message"] = "Another user has joined the match.", ["ids"] = state.registered_users}))
        else
            state.registered_users[presence.user_id] = presence
            dispatcher.broadcast_message(202, nk.json_encode({["message"] = "Someone has rejoined the match.", ["ids"] = state.registered_users}))
        end

        -- if state.registered_users and state.registered_users[presence.user_id] then
        -- -- Return the state to the player to display last updates
        -- -- The `state` must hold enough data to recreate events sequentially

        local resume_state = {
            ["registered_users"] = state.registered_users
        }

        dispatcher.broadcast_message(210, nk.json_encode({["data"] = resume_state}), {presence})

        -- end
    end

    return state
end

function M.match_leave(context, dispatcher, tick, state, presences)
    for _, presence in ipairs(presences) do
        state.presences[presence.user_id] = nil
    end

    dispatcher.broadcast_message(300, nk.json_encode({["message"] = "Opponent has disconnected."}))

    return state
end

local allow_bot = true

function M.match_loop(context, dispatcher, tick, state, messages)
    for _, message in ipairs(messages) do
        for reg_id, reg_pres in pairs(state.registered_users) do
            if reg_id == message.sender.user_id then
                nk.logger_info(nk.json_encode(message))
                dispatcher.broadcast_message(1, nk.json_encode({["data"] = "data sent"}), {message.sender})
            else
                if (tsize(state.registered_users) > 1) then
                    dispatcher.broadcast_message(2, nk.json_encode({["data"] = message.data}), {reg_pres})
                end
            end
        end
    end

    return state
end

function M.match_terminate(context, dispatcher, tick, state, grace_seconds)
    local message = ("Server shutting down in %f seconds"):format(grace_seconds)
    -- This only happens when the server is doing a graceful shutdown, such as during maintenance.
    -- NOTE: Save the match to some storage to resume later.
    dispatcher.broadcast_message(400, nk.json_encode({["data"] = message}))
    return nil
end

return M
